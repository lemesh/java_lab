package com.company.common;

import org.javatuples.Pair;

public class CircleGradle {
    public Pair<Integer, Integer> coordinates = new Pair<Integer, Integer>(5, 5);
    public double radius = 25;

    public double square() {
        return Math.PI * Math.pow(this.radius, 2);
    }
}
