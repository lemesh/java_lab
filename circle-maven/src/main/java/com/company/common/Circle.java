package com.company.common;

import org.javatuples.Pair;

public class Circle {
    public Pair<Integer, Integer> coordinates = new Pair<Integer, Integer>(0, 0);
    public double radius = 25.0;
    public double square() {
        return Math.PI * Math.pow(this.radius, 2);
    }
}
